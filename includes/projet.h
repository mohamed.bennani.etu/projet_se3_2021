#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct airlines {
  char IATA_CODE[3];
  char NOM_COMPAGNIE[50];
} airlines;

typedef struct aeroports {
  char IATA_CODE[4];
  char NOM_AEROPORT[100];
  char CITY[50];
  char STATE[50];
  char COUNTRY[20];
  float LATITUDE;
  float LONGITUDE;
} aeroports;

typedef struct flights {
  int MONTH;
  int DAY;
  char WEEKDAY[3];
  char AIRLINE[4];
  char ORG_AIR[4];
  char DEST_AIR[4];
  int SCHED_DEP;
  float DEP_DELAY;
  float AIR_TIME;
  int DIST;
  int SCHED_ARR;
  int ARR_DELAY;
  int DIVERTED; // 1 correspond a la deviation du vol
  int CANCEL;   // 0 correspond a l'annulation du vol
} flights;

typedef struct flight_chainee {
  flights vols;
  struct flight_chainee *suivant;
} flight_chainee;

typedef struct airport_chainee {
  aeroports airports;
  struct airport_chainee *suivant;
} airport_chainee;

typedef struct airline_chainee {
  airlines Airline;
  struct airline_chainee *suivant;
} airline_chainee;

void ajoutete_airl(airline_chainee **A, airlines air);
void lire_airline(FILE *f, airline_chainee **A) ;
void show_airline(airline_chainee *A);
void ajouteteaeroport2(airport_chainee **A, aeroports aero);
void lire_airport_ligne(airport_chainee **A, char *liste) ;
int lire_airport(FILE *fairp, airport_chainee **AI) ;
void show_airports(airport_chainee *A);
void ajouteteflight(flight_chainee **A, flights f); 
void ligne_flights(char *ligne, flight_chainee **fL); 
int read_flight(FILE *f, flight_chainee **F);
void show_vols(flight_chainee *FL);
int ajoutete2(airport_chainee** AIRPC,aeroports airp);
void requeteno1(char iata_airline[], flight_chainee* FLC,airport_chainee* AIRPC);
void delayed_airlines(char *iata_airline, flight_chainee *FLC,
                      airline_chainee *AIRLC);
void afficher_parametres_vols(flights f);
void changed_flights(flight_chainee *FLC, int mois, int day);
void avg_flight_duration(char* src,char* rec,flight_chainee* FLC);
void airline_iata(airlines *AIRLINE, airline_chainee *AIRLC, char iata_airline[3]);
int contientairline(char iata[3], airline_chainee *AIRLC);
void requete2(char iata_airport[4], airline_chainee *AIRLC,
              flight_chainee *FLC) ;
int rechercher_vols(flights Flight, flight_chainee *FLC);
void requete3(char iata_airport[4], int day, int month, flight_chainee *FLC, int date_hour); 
void erase_airline(airline_chainee** AIRLC);
void erase_flights(flight_chainee** FLC);
void erase_airports(airport_chainee** AIRPC);

