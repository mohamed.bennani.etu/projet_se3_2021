/* le projet pa de 2020-2021*/
/* réalisé par abdelillah elkhotri et mohamed bennani*/
#include "../includes/projet.h"
/*airport_chainee* init_airport(char* texte){
  char* partie;
  char* liste[6];
  char* ligne;
  airport_chainee* A=malloc(sizeof(airport_chainee));
  for(int i=0;i<=6;i++){
      partie=strsep(&ligne,",");
      liste[i]=partie;
  }
      strcpy(A->airports.IATA_CODE,liste[0]);
      strcpy(A->airports.NOM_AEROPORT,liste[1]);
      strcpy(A->airports.CITY,liste[2]);
      strcpy(A->airports.STATE,liste[3]);
      strcpy(A->airports.COUNTRY,liste[4]);
      strcpy(A->airports.LONGITUDE,liste[5]);
      strcpy(A->airports.LATITUDE,liste[6]);
      A->suivant=NULL;
      return A;
      }*/
void ajoutete_airl(airline_chainee **A, airlines air) {
  airline_chainee *tmp = malloc(sizeof(airline_chainee));
  tmp->Airline = air;
  tmp->suivant = (*A);
  (*A) = tmp;
}
// lecture et affichage des compagnies aériennes

void lire_airline(FILE *f, airline_chainee **A) {
  if (f == NULL) {
    return;
  }
  airline_chainee *tmp = malloc(sizeof(airline_chainee));
  if (tmp == NULL) {
    return;
  }
  while (fscanf(f, "%2s,", tmp->Airline.IATA_CODE) != EOF) {
    fscanf(f, "%[^\n]",
           tmp->Airline.NOM_COMPAGNIE); // pour lire tous les caractéres sauf le
                                        // retour a la ligne
    ajoutete_airl(A, tmp->Airline);
  }
}

void show_airline(airline_chainee *A) {
  if (A == NULL) {
    return; // pas de vols disponibles
  }
  while (A != NULL) {
    printf("%s,", A->Airline.IATA_CODE);
    printf("%s\n", A->Airline.NOM_COMPAGNIE);
    A = A->suivant;
  }
}
// lecture et affichage des aeroports

void ajouteteaeroport2(airport_chainee **A, aeroports aero) {
  airport_chainee *tmp = malloc(sizeof(airport_chainee));
  tmp->airports = aero;
  tmp->suivant = (*A);
  (*A) = tmp;
}
void lire_airport_ligne(airport_chainee **A, char *liste) {
  char *partie;
  char *ligne[7];
  airport_chainee *airp = malloc(sizeof(airport_chainee));
  for (int i = 0; i < 7; i++) {
    partie = strsep(&liste, ",");
    ligne[i] = partie;
  }
  strcpy(airp->airports.IATA_CODE, ligne[0]);
  strcpy(airp->airports.NOM_AEROPORT, ligne[1]);
  strcpy(airp->airports.CITY, ligne[2]);
  strcpy(airp->airports.STATE, ligne[3]);
  strcpy(airp->airports.COUNTRY, ligne[4]);
  airp->airports.LATITUDE = atof(ligne[5]);
  airp->airports.LONGITUDE = atof(ligne[6]);
  ajouteteaeroport2(A, airp->airports);
}
int lire_airport(FILE *fairp, airport_chainee **AI) {
  size_t n = 500;
  char *ligne = malloc(sizeof(char) * n);

  getline(&ligne, &n, fairp);
  while (getline(&ligne, &n, fairp) != -1) {
    lire_airport_ligne(AI, ligne);
  }
  free(ligne);
  return 1;
}

/*void lire_airport(FILE* f,aeroports A[]){
  size_t n=700;
  char* token=malloc(sizeof(char) * n);
  int i=0;
   getline(&token,&n,f);
  while(getline(&token,&n,f)!=EOF){
    char* c=strsep(&token,",");
    strcpy(A[i].IATA_CODE,c);
    c=strsep(&token,",");
    strcpy(A[i].NOM_AEROPORT,c);
     c=strsep(&token,",");
    strcpy(A[i].CITY,c);
    c=strsep(&token,",");
    strcpy(A[i].STATE,c);
     c=strsep(&token,",");
     strcpy(A[i].COUNTRY,c);
     c=strsep(&token,",");
     A[i].LATITUDE=atof(c);
     c=strsep(&token,",");
     A[i].LONGITUDE=atof(c);
     i++;
  }
}
void afficher_airports(aeroports A[]){
  for(int i=0;i<=321;i++){
     printf("%s,%s,%s,%s,%s,%f,%f\n",A[i].IATA_CODE,A[i].NOM_AEROPORT,A[i].CITY,A[i].STATE,A[i].COUNTRY,A[i].LATITUDE,A[i].LONGITUDE);
  }
  }*/

void show_airports(airport_chainee *A) {
  if (A == NULL) {
    printf("aucun aeroport trouvé");
    return;
  }
  while (A != NULL) {
    printf("%s,", A->airports.IATA_CODE);
    printf("%s,", A->airports.CITY);
    printf("%s,", A->airports.STATE);
    printf("%s,", A->airports.COUNTRY);
    printf("%f,", A->airports.LATITUDE);
    printf("%f,", A->airports.LONGITUDE);
    printf("\n");
    A = A->suivant;
  }
}
// lecture et affichage des vols

void ajouteteflight(flight_chainee **A, flights f) {
  flight_chainee *c = malloc(sizeof(flight_chainee));
  c->vols = f;
  c->suivant = *A;
  *A = c;
}

void ligne_flights(char *ligne, flight_chainee **fL) {
  char *partie;
  char *liste[14];
  flight_chainee *fl = malloc(sizeof(
      flight_chainee)); // flight_chainee* fch=malloc(sizeof(flight_chainee));
  for (int i = 0; i <14; i++) {
    partie = strsep(&ligne, ",");
    liste[i] = partie;
  }
  fl->vols.MONTH = atoi(liste[0]);
  fl->vols.DAY = atoi(liste[1]);
  strcpy(fl->vols.WEEKDAY, liste[2]);
  strcpy(fl->vols.AIRLINE, liste[3]);
  strcpy(fl->vols.ORG_AIR, liste[4]);
  strcpy(fl->vols.DEST_AIR, liste[5]);
  fl->vols.SCHED_DEP = atoi(liste[6]);
  fl->vols.DEP_DELAY = atof(liste[7]);
  fl->vols.AIR_TIME = atof(liste[8]);
  fl->vols.DIST = atoi(liste[9]);
  fl->vols.SCHED_ARR = atoi(liste[10]);
  fl->vols.ARR_DELAY = atoi(liste[11]);
  fl->vols.DIVERTED = atoi(liste[12]);
  fl->vols.CANCEL = atoi(liste[13]);
  ajouteteflight(fL, fl->vols);
  free(fl);
}

int read_flight(FILE *f, flight_chainee **F) {
  if (f == NULL)
    printf("nn chargé");
  size_t n = 400;
  char *ligne = malloc(sizeof(char) * n);
  getline(&ligne, &n, f);
  while (getline(&ligne, &n, f) != -1) {
    ligne_flights(ligne, F);
  }
  free(ligne);
  return 1;
}
void show_vols(flight_chainee *FL) {
  while (FL != NULL) {
    printf("%d,", FL->vols.MONTH);
    printf("%d,", FL->vols.DAY);
    printf("%s,", FL->vols.WEEKDAY);
    printf("%s,", FL->vols.AIRLINE);
    printf("%s,", FL->vols.ORG_AIR);
    printf("%s,", FL->vols.DEST_AIR);
    printf("%d,", FL->vols.SCHED_DEP);
    printf("%f,", FL->vols.DEP_DELAY);
    printf("%f,", FL->vols.AIR_TIME);
    printf("%d,", FL->vols.DIST);
    printf("%d,", FL->vols.SCHED_ARR);
    printf("%d,", FL->vols.ARR_DELAY);
    printf("%d,", FL->vols.DIVERTED);
    printf("%d", FL->vols.CANCEL);
    printf("\n");
    FL = FL->suivant;
  }
}
/*---------requetes-----------------*/
/*show_airport  // requete no 1*/

void ajoutete2(airport_chainee **AIRPC,
               aeroports airp) { // pour ajouter un aéroport sans duplication
  airport_chainee *AIRPC1 = malloc(sizeof(struct airport_chainee));
  if (AIRPC1 == NULL)
    return;
  airport_chainee *AIRPC3 = *AIRPC;
  if (AIRPC3 == NULL) {
    ajouteteaeroport2(AIRPC, airp);
    free(AIRPC1);
    return;
  }
  while (AIRPC3 != NULL) {
    AIRPC3 = AIRPC3->suivant;
  }
  AIRPC3 = *AIRPC;
  while (AIRPC3->suivant != NULL &&
         strcmp(AIRPC3->airports.IATA_CODE, airp.IATA_CODE) != 0) {
    AIRPC3 = AIRPC3->suivant;
  }
  if (strcmp(AIRPC3->airports.IATA_CODE, airp.IATA_CODE) == 0) {
    free(AIRPC1);
  }
  AIRPC1->airports = airp;
  AIRPC1->suivant = NULL;
  AIRPC3->suivant = AIRPC1;
}

void requeteno1(char iata_airline[], flight_chainee *FLC,
                airport_chainee *AIRPC) {
  airport_chainee *AIRPC1 = NULL;
  flight_chainee *FLC1 = FLC;
  airport_chainee *AIRPC2 = AIRPC;
  if (FLC1 == NULL || AIRPC == NULL) {
    printf("les aeroports et vols sont indisponibles \n");
    return;
  }
  while (FLC1 != NULL) {
    if (strcmp(FLC1->vols.AIRLINE, iata_airline)) {
      while (AIRPC2 != NULL) {
        if (strcmp(AIRPC2->airports.IATA_CODE, FLC1->vols.ORG_AIR) == 0 ||
            strcmp(AIRPC2->airports.IATA_CODE, FLC1->vols.DEST_AIR) == 0) {
          ajoutete2(&(AIRPC1), AIRPC2->airports);
        }
        AIRPC2 = AIRPC2->suivant;
      }
      // AIRPC2=AIRPC;
    }
    FLC1 = FLC1->suivant;
    AIRPC2=AIRPC;
  }
  show_airports(AIRPC1);
  printf("\n");
}

// requete no 6

void delayed_airlines(char *iata_airline, flight_chainee *FLC,
                      airline_chainee *AIRLC) {
  if (FLC == NULL || AIRLC == NULL)
    return;
  airlines AIR;
  while (AIRLC != NULL) {
    if (strcmp(AIRLC->Airline.IATA_CODE, iata_airline) == 0) {
      AIR = AIRLC->Airline;
    }
    AIRLC = AIRLC->suivant;
  }
  int retard = 0, mnb = 0;
  while (FLC != NULL) {
    if (strcmp(FLC->vols.AIRLINE, iata_airline) == 0) {
      retard += FLC->vols.ARR_DELAY;
      mnb++;
    }
    FLC = FLC->suivant;
  }
  printf("la compagnie aérienne %s a environ %d minutes de retard",
         AIR.NOM_COMPAGNIE, retard / mnb);
}
// requete no 8

void afficher_parametres_vols(flights f) {
  printf("%d,", f.MONTH);
  printf("%d,", f.DAY);
  printf("%s,", f.WEEKDAY);
  printf("%s,", f.AIRLINE);
  printf("%s,", f.ORG_AIR);
  printf("%s,", f.DEST_AIR);
  printf("%d,", f.SCHED_DEP);
  printf("%f,", f.DEP_DELAY);
  printf("%f,", f.AIR_TIME);
  printf("%d,", f.DIST);
  printf("%d,", f.SCHED_ARR);
  printf("%d,", f.ARR_DELAY);
  printf("%d,", f.DIVERTED);
  printf("%d", f.CANCEL);
  printf("\n");
}
void changed_flights(flight_chainee *FLC, int mois, int day) {
  if (FLC == NULL) {
    return;
  }
  while (FLC != NULL) {
    if (FLC->vols.MONTH == mois && FLC->vols.DAY == day &&
        (FLC->vols.DIVERTED != 0 || FLC->vols.CANCEL != 0)) {
      afficher_parametres_vols(FLC->vols);
    }
    FLC = FLC->suivant;
  }
}

//

void avg_flight_duration(char *src, char *rec, flight_chainee *FLC) {
  if (FLC == NULL) {
    printf("pas de vols trouvés");
    return;
  }
  float moyr_time = 0;
  int c = 0;
  while (FLC != NULL) {
    if (strcmp(FLC->vols.DEST_AIR, rec) == 0 &&
        strcmp(FLC->vols.ORG_AIR, src) == 0) {
      moyr_time = moyr_time + FLC->vols.AIR_TIME;
      c++;
    }
    FLC = FLC->suivant;
  }
  printf("moyenne:%2f min(%d vols)\n\n", moyr_time / c, c);
}

/*show_airline*/
void airline_iata(airlines *AIRLINE, airline_chainee *AIRLC,
                  char *iata_airline) {
  airlines A;
  while (AIRLC != NULL) {
    A = AIRLC->Airline;
    if (strcmp(iata_airline, A.IATA_CODE) == 0)
      *AIRLINE = A;
    AIRLC = AIRLC->suivant;
  }
}
int contientairline(char iata[3], airline_chainee *AIRLC) {
  while (AIRLC != NULL) {
    if (strcmp(iata, AIRLC->Airline.IATA_CODE) == 0)
      return 1;
    AIRLC = AIRLC->suivant;
  }
  return 0;
}
void requete2(char iata_airport[4], airline_chainee *AIRLC,
              flight_chainee *FLC) {
  airline_chainee *liste = NULL;
  airlines A;
  while (FLC != NULL) {
    if (strcmp(iata_airport, FLC->vols.ORG_AIR) == 0) {
      if (contientairline(FLC->vols.AIRLINE, liste) == 0) {
        airline_iata(&A, AIRLC, FLC->vols.AIRLINE);
      }
    }
    FLC = FLC->suivant;
  }
  show_airline(liste);
}
// requete3
int rechercher_vols(flights Flight, flight_chainee *FLC) {
  while (FLC != NULL) {
    flights f = FLC->vols;
    if (f.MONTH == Flight.MONTH && f.DAY == Flight.DAY &&
        f.WEEKDAY == Flight.WEEKDAY && f.SCHED_DEP == Flight.SCHED_DEP &&
        f.DIST == Flight.DIST && Flight.SCHED_ARR == f.SCHED_ARR) {
      return 1;
      FLC = FLC->suivant;
    }
  }
  return 0;
}

void requete3(char iata_airport[4], int day, int month, flight_chainee *FLC,
              int date_hour) {
  flight_chainee *l = NULL;
  while (FLC != NULL) {
    flights Flight = FLC->vols;
    if (strcmp(iata_airport, FLC->vols.ORG_AIR) == 0 &&
        FLC->vols.MONTH == month && FLC->vols.DAY == day &&
        (FLC->vols.SCHED_DEP) > date_hour) {
      if (rechercher_vols(Flight, l)) {
        ajouteteflight(&l, Flight);
      }
      FLC = FLC->suivant;
    }
  }
  show_vols(l);
}
void erase_airlines(airline_chainee **AIRLC) {
  if (AIRLC == NULL) {
    return;
  }
  airline_chainee *tmp = NULL; // malloc(sizeof(airline_chainee));
  while (*AIRLC != NULL) {
    tmp = (*AIRLC)->suivant;
    free(*AIRLC);
    *AIRLC = tmp;
  }
}

void erase_flights(flight_chainee **FLC) {
  if (*FLC == NULL) {
    return;
  }
  flight_chainee *x = NULL;
  while (*FLC != NULL) {
    x = (*FLC)->suivant;
    free(*FLC);
    *FLC = x;
  }
}
void erase_airports(airport_chainee **AIRPC) {
  if (*AIRPC == NULL) {
    return;
  }
  airport_chainee *x = NULL;
  while (*AIRPC != NULL) {
    x = (*AIRPC)->suivant;
    free(*AIRPC);
    *AIRPC = x;
  }
}

int main() {
  int choix;
  FILE *f = fopen("../data/data_airlines.csv", "r");
  FILE *f3 = fopen("../data/data_flights.csv", "r");
  FILE *faip = fopen("../data/data_airports.csv", "r");
  // aeroports *a = malloc(sizeof(aeroports));
  printf("///Bienvenue dans notre application des vols en USA /// \n");
  printf("\n");
  printf("les requetes disponible pour notre application sont:\n"
         "* show_airports <airline_id>\n"
         "* show_airlines_<airport_id>\n"
         "* show_flights <airport_id> <date> [time] [limit=<xx>]\n"
         "* most_delayed_flights\n"
         "* most_delayed_airlines\n"
         "* most delayed_airlines <airline_id>\n"
         "* most delayed_airline <airline_id>\n"
         "* most delayed_airline_at_airport <airline_id>\n"
         "* changed_flights <date>\n"
         "* avg_flight_duration <port_id> <port_id> calcul du temps moyen "
         "entre deux aeroports\n"
         "* find_itinerary <port_id> <port_id> <date> [<time>] [limit=<xxx>]\n"
         "* find_multicity_itinerary <port_id_depart> <>\n"
         "quit\n");
  if (f != NULL && faip != NULL && f3 != NULL) {
    printf("fichier chargés\n");
  }

  else {
    printf("une erreur d'ouverture des fichiers\n");
  }
  airline_chainee *A = NULL;
  flight_chainee *FL = NULL;
  airport_chainee *AI = NULL;
  lire_airport(faip, &AI);
  lire_airline(f, &A);
  read_flight(f3, &FL);
  printf("veuillez faire votre choix\n"
         "1 pour afficher tous les aéroports\n"
         "2 pour afficher toutes les compagnies aériennes \n"
         "3 pour afficher tous les vols \n"
         "4 pour requete 1\n"
         "5 delayed_airlines \n"
         "6 pour avg_flight_duration \n"
         "7 pour changed_flights \n"
         "8 pour requete3\n"
         "9 pour requete2\n");
  scanf("%d", &choix);
  if (choix == 1) {
    printf("tous les aéroports du fichier airports.csv sont:\n");
    show_airports(
        AI); // pour afficher tous les aéroports du fichier airports.csv
  }
  if (choix == 2) {
    printf("toutes les compagnies du fichier airlines.csv sont:\n");
    show_airline(A); // pour afficher toutes les compagnies aériennes
  }
  if (choix == 3) {
    printf("toutes les compagnies du fichier flights.csv sont:\n");
    show_vols(FL); // pour afficher tous les vols du fichier flights.csv
  }
  if (choix == 4) {
    requeteno1("F9", FL, AI);
  }
  if (choix == 5) {
    delayed_airlines("HA", FL, A);
  }
  if (choix == 6) {
    avg_flight_duration("LAX", "SFO", FL);
  }
  if (choix == 7) {
    changed_flights(FL, 1, 26);
  }
  if (choix == 8) {
    requete3("ATL", 26, 2, FL, 500); // ne marche pas
  }
  if (choix == 9) {
    requete2("ATL", A, FL);
  }
  // airport_iata(a,"ABE",AI);

  // show_airports(AI);
  //

  // read_flight(f3,&FL);
  //  show_vols(FL);

  //  show_airline(A);
  //  lire_airport(faip,&AI);
  // lire_airport(f1,AE);
  printf("\n");
  erase_airports(&AI);
  erase_flights(&FL);
  erase_airlines(&A);
  fclose(faip);
  fclose(f);
  fclose(f3);
  return 0;
}
